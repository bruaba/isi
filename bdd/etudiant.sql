-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 22 août 2020 à 13:32
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP :  7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `isi`
--

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `id` int(11) NOT NULL,
  `Matricule` varchar(255) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  `Prenom` varchar(255) NOT NULL,
  `Classe` varchar(255) NOT NULL,
  `Lieu_de_naissance` varchar(255) NOT NULL,
  `Date_de_naissance` date NOT NULL,
  `Telephone` varchar(255) NOT NULL,
  `Adresse` varchar(255) NOT NULL,
  `Annee_academique` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`id`, `Matricule`, `Nom`, `Prenom`, `Classe`, `Lieu_de_naissance`, `Date_de_naissance`, `Telephone`, `Adresse`, `Annee_academique`) VALUES
(1, 'Et-20200822-#1', 'Badiane', 'Khadija', 'Génie Logiciel', 'Dakar', '2000-03-24', '771234567', 'PA', '2019-2020'),
(2, 'Et-20200822-#2', 'marone', 'cheikh', 'Maintenance', 'dakar', '2020-08-05', '772712085', '4 rue de bretagne', '2019-2020'),
(16, 'Et-20200822-#16', 'hassan', 'diko', 'Genie Logiciel', 'thiam', '2020-07-30', '772712085', '4 rue de bretagne', '2019-2020'),
(17, 'Et-20200822-#17', 'hassan', 'diko', 'Genie Logiciel', 'thiam', '2020-07-30', '772712085', '4 rue de bretagne', '2019-2020'),
(18, 'Et-20200822-#18', 'hassan', 'diko', 'Maintenance', 'thiam', '2020-07-30', '772712085', '4 rue de bretagne', '2019-2020');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `etudiant`
--
ALTER TABLE `etudiant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
