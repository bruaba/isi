<?php

// Chargement des classes
require_once('../model/etudiant.php');

$model = new Etudiant();

$donnees = $model->getListEtudiant();

foreach($donnees as $cle => $donnee)
{
    $donnees[$cle]['Nom'] = htmlspecialchars($donnee['Nom']);
    $donnees[$cle]['id'] = htmlspecialchars($donnee['id']);
    $donnees[$cle]['Prenom'] = htmlspecialchars($donnee['Prenom']);
    $donnees[$cle]['Classe'] = nl2br(htmlspecialchars($donnee['Classe']));
    $donnees[$cle]['Matricule'] = nl2br(htmlspecialchars($donnee['Matricule']));
}

include_once "../view/list.php";

?>