<?php

// Chargement des classes
require_once('../model/etudiant.php');

$model = new Etudiant();

if (isset($_POST["matricule"])) {

	$matricule =htmlspecialchars($_POST["matricule"]);
	$donnees = $model->getInfoEtudiant($matricule);
	include_once '../view/reinscription.php';


}
elseif (isset($_POST["RegistrationN"])) {

		$model->updateInfoEtudiant($_POST['RegistrationN'], $_POST['Phone'],$_POST['ClassE'],$_POST['Address'],$_POST['AcademicY']);
		header("Location: list");	
}
else{

	header("Location: ../view/registration.php");
}

?>