<?php
/**
 * classe Etudiant 
 */
class Etudiant
{
	/**
	* Connexion à la base de donnée isi
	*/
	private function getBdd() 
	{
		$db = new PDO('mysql:host=localhost;dbname=isi;charset=utf8', 'test_user', '',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
		return $db;
	}

	/**
	* methode getListEtudiant() qui permet de récuperer la liste des étudiant
	*/

	public function getListEtudiant()
	{
		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		$req = $db->prepare('SELECT id, Matricule, Nom, Prenom, Classe FROM etudiant');
		$req->execute();
		$list_etudiant = $req->fetchAll();

		return $list_etudiant;
	}

	/**
	* methode getInfoEtudiant() qui permet de récuperer tout les informations sur l'étudiant selon le matricule
	*/

	public function getInfoEtudiant($matricule)
	{
		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		$req = $db->prepare('SELECT * FROM etudiant WHERE Matricule = ?');
		$req->execute(array($matricule));
		$info_etudiant = $req->fetchAll();

		return $info_etudiant;
	}


	/**
	* methode setInfoEtudiant() qui permet d'insérer les d'un nouveau étudiant
	* @param $NameL: le nom de l'étudiant ,
	* @param $NameF: le Prenom de l'étudiant ,
	* @param $BirthP: le lieu de naissance de l'étudiant ,
	* @param $BirthD: le date de naissance de l'étudiant ,
	* @param $Phone: le téléphone de l'étudiant ,
	* @param $ClassE: la classe de l'étudiant ,
	* @param $Address: l'adresse de l'étudiant ,
	* @param $AcademicY: le nom de l'étudiant ,
	*/

	public function setInfoEtudiant($NameL, $NameF, $BirthP ,$BirthD,$Phone,$ClassE,$Address,$AcademicY)
	{

		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}

		$req = $db->prepare('SELECT id FROM etudiant ORDER BY id DESC LIMIT 1');
		$req->execute();
		$id = $req->fetchAll();
		$lid = (int) $id[0]["id"] + 1;
		$matricule = "Et-".date("Ymd")."-#".$lid;

		$tab = 'INSERT INTO etudiant(Matricule, Nom, Prenom, Classe, Lieu_de_naissance, Date_de_naissance, Telephone, Adresse, Annee_academique) values ('.'"'.$matricule.'","'.$NameL.'","'.$NameF.'","'.$ClassE.'","'.$BirthP.'","'.$BirthD.'","'.$Phone.'","'.$Address.'","'.$AcademicY.'")';

		$req = $db->prepare($tab);
		$req->execute();
		
		return $req;
	}


	/**
	* methode updateInfoEtudiant() qui permet de mettre à jour les information d'un nouveau étudiant
	* @param $Phone: le téléphone de l'étudiant ,
	* @param $ClassE: la classe de l'étudiant ,
	* @param $Address: l'adresse de l'étudiant ,
	* @param $AcademicY: le nom de l'étudiant ,
	*/

	public function updateInfoEtudiant($RegistrationN, $Phone,$ClassE,$Address,$AcademicY)
	{

		try
		{
			$db = $this->getBdd();
		}
		catch(Exception $e)
		{
		    die('Erreur : '.$e->getMessage());
		}
		
		$tab = 'UPDATE etudiant SET Telephone = "'.$Phone.'", Adresse = "'.$Address.'", Annee_academique = "'.$AcademicY.'" WHERE Matricule =  "'.$RegistrationN.'"';

		$req = $db->prepare($tab);
		$req->execute();
		
		return $req;
	}

}

?>