<!DOCTYPE html>
<html lang="en">
<head>
	<title>Contact V5</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="view/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="view/css/util.css">
	<link rel="stylesheet" type="text/css" href="view/css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">

			
			<?php
			foreach ($donnees as $donnee) 
                 {
                        ?>
				<span class="contact100-form-title">
					Re Registration
				</span>
				

				<div class="wrap-input100 bg1">
					<span class="label-input100">Registration Number</span>

					<div class="input-group">
                     <input class="input100" type="text" name="matricule" value="<?php echo $donnee['Matricule']; ?>" disabled>
                     	<div class="input-group-btn">
                     		<a href="registration">
                     			<button class="contact100-form-btn">Switch</button>
                     		</a>
                     	</div>
                    </div>
				
				</div>

				
				<form  action="reinscription" method="post" enctype="multipart/form-data"class="contact100-form validate-form">
				<input class="input100" type="hidden" name="RegistrationN" value="<?php echo $donnee['Matricule']; ?>">
				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Please Type Your Last Name">
					<span class="label-input100">Last Name</span>
					<input class="input100" type="text" value="<?php echo $donnee['Nom']; ?>" disabled>
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Please Type Your Place of birth">
					<span class="label-input100">Place of birth</span>
					<input class="input100" type="text" value="<?php echo $donnee['Lieu_de_naissance']; ?>" disabled>
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Please Type Your First Name">
					<span class="label-input100">First Name</span>
					<input class="input100" type="text" value="<?php echo $donnee['Prenom']; ?>" disabled>
				</div>

				<div class="wrap-input100 bg1 rs1-wrap-input100">
					<span class="label-input100">Phone</span>
					<input class="input100" type="text" name="Phone" value="<?php echo $donnee['Telephone']; ?>">
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Please Type Your Date of birth">
					<span class="label-input100">Date of birth</span>
					<input class="input100" type="text" value="<?php echo $donnee['Date_de_naissance']; ?>" disabled>
				</div>

				<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
					<span class="label-input100">Class</span>
					<div>
						<select class="js-select2" name="ClassE">
							<option>Please choose</option>
							<option value="Genie Logiciel">Genie Logiciel</option>
							<option value="Maintenance">Maintenance</option>
						</select>
						<div class="dropDownSelect2"></div>
					</div>
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100">
					<span class="label-input100">Address</span>
					<input class="input100" type="text" name="Address" value="<?php echo $donnee['Adresse']; ?>">
				</div>

				<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
					<span class="label-input100">Academic year</span>
					<div>
						<select class="js-select2" name="AcademicY">
							<option value="2019-2020" selected>2019 - 2020</option>
							<option value="2020-2021">2020 - 2021</option>
						</select>
						<div class="dropDownSelect2"></div>
					</div>
				</div>

				<div class="container-contact100-form-btn">
					<button value="edit" class="contact100-form-btn">
						<span>
							Re Registration
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>

			<a href="list">
				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							List of students
							<i class="fa fa-list m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</a>
			<?php

		}
		?>
			
		</div>
	</div>



<!--===============================================================================================-->
	<script src="view/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="view/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="view/vendor/bootstrap/js/popper.js"></script>
	<script src="view/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="view/vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});


			$(".js-select2").each(function(){
				$(this).on('select2:close', function (e){
					if($(this).val() == "Please chooses") {
						$('.js-show-service').slideUp();
					}
					else {
						$('.js-show-service').slideUp();
						$('.js-show-service').slideDown();
					}
				});
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="view/vendor/daterangepicker/moment.min.js"></script>
	<script src="view/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="view/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="view/vendor/noui/nouislider.min.js"></script>
	<script>
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 1500, 3900 ],
	        connect: true,
	        range: {
	            'min': 1500,
	            'max': 7500
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]);
	        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
	        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
	    });
	</script>
<!--===============================================================================================-->
	<script src="view/js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
